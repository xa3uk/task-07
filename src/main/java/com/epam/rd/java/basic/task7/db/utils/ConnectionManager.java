package com.epam.rd.java.basic.task7.db.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static Connection con;
    private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

    public static Connection getConnection() {
        if (con != null) {
            return con;
        }
        try {
            con = DriverManager.getConnection(CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }
}
