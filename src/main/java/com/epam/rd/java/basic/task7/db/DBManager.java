package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.epam.rd.java.basic.task7.db.utils.ConnectionManager;


public class DBManager {

    private static DBManager instance;
    Connection con = ConnectionManager.getConnection();

    public static synchronized DBManager getInstance() {
        if (instance != null) {
            return instance;
        }
        return new DBManager();
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Statement st = null;
        try {
            st = con.createStatement();  // use try with resources
            ResultSet rs = st.executeQuery("select * from users");
            while (rs.next()) {
                int id = rs.getInt("id");
                String login = rs.getString("login");
                User user = new User();
                user.setId(id);
                user.setLogin(login);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        String sql = "INSERT INTO users (login) VALUES (?)";
        PreparedStatement st = null;
        int execute = 0;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, user.getLogin());
            execute = st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        user.setId(getUserId(user));
        return execute > 0;
    }

    public boolean deleteUsers(User... users) throws DBException {
        PreparedStatement st = null;
        int execute = 0;
        for (User user : users) {
            String login = user.getLogin();
            try {
                st = con.prepareStatement("delete from users where login=?");
                st.setString(1, login);
                execute = st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return execute > 0;
    }

    public User getUser(String login) throws DBException {
        String sql = "select * from users where login = ?";
        PreparedStatement st = null;
        User user = null;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, login);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        String sql = "select * from teams where name = ?";
        PreparedStatement st = null;
        Team team = null;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, name);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        Statement st = null;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from teams");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Team team = new Team();
                team.setId(id);
                team.setName(name);
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        String sql = "INSERT INTO teams (name) VALUES (?)";
        PreparedStatement st = null;
        int execute = 0;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, team.getName());
            execute = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        team.setId(getTeamId(team));
        return execute > 0;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
        int userId = getUserId(user);
        Connection connection = null;
        try {
            connection = ConnectionManager.getConnection();

            PreparedStatement st = connection.prepareStatement(sql);
            connection.setAutoCommit(false);
            for (Team team : teams) {
                int teamId = getTeamId(team);
                st.setInt(1, userId);
                st.setInt(2, teamId);
                st.addBatch();
            }
            st.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException("error", e.getCause());
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String sql = "select * from users_teams where user_id = ?";
        PreparedStatement st = null;
        int userId = getUserId(user);
        List<Team> teamList = new ArrayList<>();
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                int teamId = rs.getInt("team_id");
                String teamName = getTeamNameById(teamId);
                team.setId(teamId);
                team.setName(teamName);
                teamList.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teamList;
    }


    public boolean deleteTeam(Team team) throws DBException {
        PreparedStatement st = null;
        String teamName = team.getName();
        int execute = 0;
        try {
            st = con.prepareStatement("delete from teams where name = ?");
            st.setString(1, teamName);
            execute = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return execute > 0;
    }

    public boolean updateTeam(Team team) throws DBException {
        int teamId = team.getId();
        String teamName = team.getName();
        String sql = "update teams set name = ? where id = ?";
        PreparedStatement st = null;
        int execute = 0;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, teamName);
            st.setInt(2, teamId);
            execute = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return execute > 0;
    }

    private int getUserId(User user) throws DBException {
        String sql = "select * from users where login = ?";
        PreparedStatement st = null;
        int id = 0;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, user.getLogin());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    private int getTeamId(Team team) throws DBException {
        String sql = "select * from teams where name = ?";
        PreparedStatement st = null;
        int id = 0;
        try {
            st = con.prepareStatement(sql);
            st.setString(1, team.getName());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String getTeamNameById(int id) throws DBException {
        String sql = "select * from teams where id = ?";
        PreparedStatement st = null;
        String teamName = null;
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                teamName = rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teamName;
    }
}
